#pragma once

#include <functional>
#include <list>
#include <unordered_map>

namespace LHCb
{
  namespace Utils
  {
    /// Implementation of a trivial
    template <class Key, class T, class Hash = std::hash<Key>, class KeyEqual = std::equal_to<Key>>
    class LRUCache
    {
      using Store = std::unordered_map<Key, T, Hash, KeyEqual>;
      using Ages  = std::list<Key>;

    public:
      using size_type = typename Store::size_type;

      LRUCache( size_type capacity, size_type remove_size = 0 ) : m_capacity( capacity )
      {
        // by default free 1/4 of the capacity when needed
        if ( !remove_size ) remove_size = std::max( m_capacity >> 2, static_cast<size_type>( 1 ) );
        // at most wipe the content when needed
        if ( remove_size >= m_capacity )
          m_lowMark = 0;
        else
          m_lowMark = m_capacity - remove_size;
      }

      const T& at( const Key& key )
      {
        const T& value = m_store.at( key );
        promote( key );
        return value;
      }

      const T& add( const Key& key, T value )
      {
        check_size();
        auto retval = m_store.emplace( key, std::move( value ) );
        promote( key );
        return retval.first->second;
      }

      size_type count( const Key& key ) { return m_store.count( key ); }

      void clear()
      {
        m_store.clear();
        m_ages.clear();
      }

      size_type size() const { return m_store.size(); }

    private:
      /// update internal structure to record that the key has been accessed
      void promote( const Key& key )
      {
        const auto end_marker = end( m_ages );
        auto item             = std::find( begin( m_ages ), end_marker, key );
        if ( item != end_marker ) m_ages.erase( item );
        m_ages.emplace_front( key );
      }

      void check_size()
      {
        if ( m_store.size() >= m_capacity ) {
          // we need space
          if ( m_lowMark == 0 ) {
            // shortcut for full wipe out
            clear();
          } else {
            const auto pos        = std::next( begin( m_ages ), m_lowMark );
            const auto end_marker = end( m_ages );
            for ( auto i = pos; i != end_marker; ++i ) {
              m_store.erase( *i );
            }
            m_ages.erase( pos, end_marker );
          }
        }
      }

      Store m_store;
      Ages m_ages;

      size_type m_capacity, m_lowMark;
    };
  }
}
