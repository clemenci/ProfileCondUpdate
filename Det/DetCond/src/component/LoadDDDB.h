#pragma once

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class LoadDDDB LoadDDDB.h
 *
 *  Load entries in the detector transient store using IDataSvc::preLoad().
 *  The node to be loaded is set with the option LoadDDDB.Node.
 *
 *  @author Marco Clemencic
 *  @date   2005-10-14
 */
class LoadDDDB : public GaudiAlgorithm {
public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_treeToLoad{this, "Node", "/dd*"};
};
