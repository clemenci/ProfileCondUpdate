#include "FakeEventTime.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiKernel/IIncidentSvc.h"

class BackForthFakeEventTime : public FakeEventTime {
public:
  using FakeEventTime::FakeEventTime;

protected:
  void i_increment() override {
    FakeEventTime::i_increment();
    m_timeStep = -m_timeStep;
  }
};

DECLARE_COMPONENT( BackForthFakeEventTime )
