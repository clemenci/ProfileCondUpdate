from DetDescChecks.Options import LoadDDDBTest
LoadDDDBTest('2017')

from Configurables import ApplicationMgr
app = ApplicationMgr()
app.EvtMax = 10

from Configurables import EventClockSvc, BackForthFakeEventTime
from Gaudi.Configuration import DEBUG
ecs = EventClockSvc()
ecs.InitialTime = 1501545600000000000 - int(1e9)
ecs.addTool(BackForthFakeEventTime, 'FakeEventTime')
ecs.FakeEventTime.TimeStep = int(2e9)
ecs.EventTimeDecoder = ecs.FakeEventTime
ecs.OutputLevel = DEBUG
