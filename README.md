This small project was created to study performance issues with conditions
updates between events.

To run the test, build and call
```
./run gaudirun.py force_conditions_updates.py
```
This loads the whole detector description on the first event, then it changes
the event time back and forth at every event, around an IOV boundary
(2017-08-01 00:00:00 UTC) of the Online alignment conditions.

The script `do_profile.sh` runs callgrind on the test job to profile the work
done during an update of the conditions.
