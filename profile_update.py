from Configurables import ApplicationMgr
app = ApplicationMgr()

from Configurables import CallgrindProfile
p = CallgrindProfile('CallgrindProfile')
p.StartFromEventN = 4
p.StopAtEventN = 5
p.DumpAtEventN = 5
p.DumpName = 'CALLGRIND-OUT'
app.TopAlg.append(p)
app.EvtMax = p.DumpAtEventN
